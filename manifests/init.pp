define mac-defaults-user (
	$domain,
	$key,
	$type,
	$value
) {
	$params = split($name, ',')
	$user = $params[0]

	$user_domain = "/Users/${user}${domain}"

	Exec <| title == "defaults write $user_domain $key -$type '$value'" |> {
		user => $user,
	}

	if ($user != 'root') and ($user != 'Shared') {
		mac-defaults { "${user}:${domain}:${key}":
      domain => $user_domain,
      key    => $key,
      type   => $type,
      value  => $value,
		}
	}
}

define mac-defaults-all-users (
	$domain,
	$key,
	$type,
	$value
) {
	if $users != '' {
		$users_split = split($users, "\n")

		$users_split_with_domain_key = regsubst($users_split, "\$", ",${domain}/${key}")

		mac-defaults-user { $users_split_with_domain_key:
			domain => $domain,
			key    => $key,
			type   => $type,
			value  => $value
		}

	}
}

class mac {
	User <| |> {
		managehome => false,
	}
}
